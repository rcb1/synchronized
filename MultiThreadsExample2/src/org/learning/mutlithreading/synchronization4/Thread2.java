package org.learning.mutlithreading.synchronization4;

public class Thread2 extends Thread {

	private int n;
	AddAndSubImpl addAndSub;

	Thread2(int n, AddAndSubImpl aas) {
		addAndSub = aas;
		this.n = n;
	}

	public void run() {
		synchronized (addAndSub) {
			addAndSub.subTable(n);
		}
	}
}
