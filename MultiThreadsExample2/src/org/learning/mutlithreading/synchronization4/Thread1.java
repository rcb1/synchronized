package org.learning.mutlithreading.synchronization4;

public class Thread1 extends Thread {
	AddAndSubImpl addAndSub;
	private int n;

	Thread1(int n, AddAndSubImpl aas) {
		addAndSub = aas;
		this.n = n;
	}

	public void run() {
		synchronized (addAndSub) {
			addAndSub.addTable(n);
		}
	}
}
