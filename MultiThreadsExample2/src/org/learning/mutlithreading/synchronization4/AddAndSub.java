package org.learning.mutlithreading.synchronization4;

public interface AddAndSub {

	public int add(int a, int b);

	public int sub(int a, int b);

	public int addTable(int n);

	public int subTable(int n);
}
