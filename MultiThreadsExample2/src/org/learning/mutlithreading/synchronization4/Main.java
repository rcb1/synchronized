package org.learning.mutlithreading.synchronization4;

import java.util.Scanner;

public class Main {

	public static void main(String args[]) {
		AddAndSubImpl addAndSub = new AddAndSubImpl();
		Scanner s = new Scanner(System.in);
		System.out.print("Enter 1st number: ");
		int a = s.nextInt();
		System.out.print("Enter 2nd number: ");
		int b = s.nextInt();
		s.close();
		Thread t1 = new Thread1(addAndSub.add(a, b), addAndSub);
		Thread t2 = new Thread2(addAndSub.sub(a, b), addAndSub);

		t1.start();
		t2.start();

		try {
			t1.join();
			t2.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
