package org.learning.mutlithreading.synchronization4;

public class AddAndSubImpl implements AddAndSub {

	@Override
	public int add(int a, int b) {
		int c = 2 * (a + b);
		return c;
	}

	@Override
	public int sub(int a, int b) {
		int c = 5 * (a - b);
		return c;
	}

	@Override
	public int addTable(int n) {
		for (int i = 1; i <= 10; i++) {
			System.out.println(n + " * " + i + " = " + (n * i));
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return n;
	}

	@Override
	public int subTable(int n) {
		if (!(n < 0)) {
			for (int i = 1; i <= 10; i++) {
				System.out.println(n + " * " + i + " = " + (n * i));
				try {
					Thread.sleep(1000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else
			System.out.println("Difference is a negative value, table cannot be printed");
		return n;
	}

}
